package host.apppf.api.service;

import host.apppf.api.factory.AnalyticsFactory;
import host.apppf.api.factory.ProductsFactory;
import host.apppf.api.factory.StatisticsFactory;
import host.apppf.api.model.RequestModel;
import host.apppf.api.model.ResponseModel;
import host.apppf.api.model.TaskModel;

import java.util.Random;

public class MakeReportTask implements Runnable {

    private TaskModel taskModel;
    private RequestModel requestModel;

    public MakeReportTask(TaskModel taskModel, RequestModel requestModel) {
        this.taskModel = taskModel;
        this.requestModel = requestModel;
    }

    @Override
    public void run() {
        taskModel.setStatus(ResponseModel.StatusReport.processing);
        StringBuilder data = new StringBuilder();
        // Общее время выполнения от 30 сек до 2 мин
        Random r = new Random();
        long sleep = r.nextInt(900) + 300;
        // Кол-во записей от 1К до 10К
        int count = r.nextInt(90) + 10;
        for (int i = 0; i <= 100; i++) {
            taskModel.setPercent(i);
            switch (requestModel.getType()) {
                case products:
                    data.append(ProductsFactory.make(count));
                    break;
                case analytics:
                    data.append(AnalyticsFactory.make(count));
                    break;
                case statistics:
                    data.append(StatisticsFactory.make(count));
                    break;
            }
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        taskModel.setData(data.toString());
        taskModel.setStatus(ResponseModel.StatusReport.completed);
    }
}
