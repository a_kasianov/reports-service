package host.apppf.api.service;

import host.apppf.api.model.RequestModel;
import host.apppf.api.model.ResponseModel;
import host.apppf.api.model.TaskModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class ReportService {

    private AtomicInteger identify = new AtomicInteger();
    private final List<TaskModel> poolTasks = Collections.synchronizedList(new ArrayList<>());
    private ExecutorService executor = Executors.newCachedThreadPool();

    public ResponseModel addTask(RequestModel request) {
        if (!authOperator(request.getOperatorId(), request.getOperatorKey())) {
            return new ResponseModel(false, "Auth error", 0);
        }
        TaskModel taskModel = new TaskModel(identify.incrementAndGet());
        poolTasks.add(taskModel);
        executor.submit(new MakeReportTask(taskModel, request));
        return new ResponseModel(true, "Task created", taskModel.getId());
    }

    public ResponseModel getStatus(RequestModel request) {
        if (!authOperator(request.getOperatorId(), request.getOperatorKey())) {
            return new ResponseModel(false, "Auth error", 0);
        }
        ResponseModel response = new ResponseModel();
        synchronized (poolTasks) {
            for (TaskModel task: poolTasks) {
                if (task.getId() == request.getReportId()) {
                    response.setSuccess(true);
                    response.setStatus(task.getStatus());
                    response.setPercent(task.getPercent());
                    response.setReportId(task.getId());
                    return response;
                }
            }
        }
        response.setSuccess(false);
        response.setMessage("Task not found");
        return response;
    }

    public String getReport(int id) {
        synchronized (poolTasks) {
            for (TaskModel task: poolTasks) {
                if (task.getId() == id && task.getStatus() == ResponseModel.StatusReport.completed) {
                    poolTasks.remove(task);
                    return task.getData();
                }
            }
        }
        return null;
    }

    private boolean authOperator(String operatorId, String operatorKey) {
        return operatorId.equals("operator001") && operatorKey.equals("secret");
    }
}
