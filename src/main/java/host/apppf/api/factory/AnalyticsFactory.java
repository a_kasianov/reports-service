package host.apppf.api.factory;

import au.com.anthonybruno.Gen;
import au.com.anthonybruno.settings.CsvSettings;
import com.github.javafaker.Faker;

import java.util.Locale;

public class AnalyticsFactory {

    private static final Faker faker = new Faker(new Locale("ru_RU"));
    private static final CsvSettings csvSettings = new CsvSettings.Builder().setDelimiter(';').build();

    public static String make(int count) {
        return Gen.start()
                .addField("company", () -> faker.company().name())
                .addField("url", () -> faker.company().url())
                .addField("address", () -> faker.address().streetAddress())
                .addField("phone", () -> faker.phoneNumber().cellPhone())
                .addField("order_count", () -> faker.number().numberBetween(1000, 1000000))
                .addField("credit_card", () -> faker.finance().creditCard())
                .generate(count)
                .asCsv(csvSettings)
                .toStringForm();
    }
}
