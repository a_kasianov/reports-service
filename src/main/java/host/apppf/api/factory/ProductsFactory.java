package host.apppf.api.factory;

import au.com.anthonybruno.Gen;
import au.com.anthonybruno.settings.CsvSettings;
import com.github.javafaker.Faker;

import java.util.Locale;

public class ProductsFactory {

    private static final Faker faker = new Faker(new Locale("ru_RU"));
    private static final CsvSettings csvSettings = new CsvSettings.Builder().setDelimiter(';').build();

    public static String make(int count) {
        return Gen.start()
                .addField("product", () -> faker.food().fruit())
                .addField("measurement", () -> faker.food().measurement())
                .addField("currency", () -> faker.currency().code())
                .addField("price", () -> faker.number().numberBetween(1, 1000))
                .generate(count)
                .asCsv(csvSettings)
                .toStringForm();
    }
}
