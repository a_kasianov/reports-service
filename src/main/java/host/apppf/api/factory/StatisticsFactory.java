package host.apppf.api.factory;

import au.com.anthonybruno.Gen;
import au.com.anthonybruno.settings.CsvSettings;
import com.github.javafaker.Faker;

import java.util.Locale;

public class StatisticsFactory {

    private static final Faker faker = new Faker(new Locale("ru_RU"));
    private static final CsvSettings csvSettings = new CsvSettings.Builder().setDelimiter(';').build();

    public static String make(int count) {
        return Gen.start()
                .addField("first_name", () -> faker.name().firstName())
                .addField("last_name", () -> faker.name().lastName())
                .addField("address", () -> faker.address().streetAddress())
                .addField("phone", () -> faker.phoneNumber().cellPhone())
                .addField("order_id", () -> faker.number().numberBetween(1000, 1000000))
                .addField("amount", () -> faker.number().numberBetween(10, 1000))
                .generate(count)
                .asCsv(csvSettings)
                .toStringForm();
    }
}
