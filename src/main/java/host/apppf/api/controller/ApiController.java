package host.apppf.api.controller;

import host.apppf.api.model.RequestModel;
import host.apppf.api.model.ResponseModel;
import host.apppf.api.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ApiController {

    private ReportService reportService;

    @Autowired
    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    @PostMapping("/reports/status")
    public ResponseModel status(@Valid @RequestBody RequestModel request) {
        return reportService.getStatus(request);
    }

    @PostMapping("/reports/request")
    public ResponseModel create(@Valid @RequestBody RequestModel request) {
        return reportService.addTask(request);
    }

    @GetMapping("/reports/{id}_report.csv")
    public ResponseEntity<String> get(@PathVariable("id") int id) {
        String result = reportService.getReport(id);
        if (result == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok()
                .body(result);
    }
}
