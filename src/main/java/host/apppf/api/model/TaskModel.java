package host.apppf.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class TaskModel {
    private int id;
    private ResponseModel.StatusReport status;
    private Date created;
    private int percent;
    private String data;

    public TaskModel(int id) {
        this.id = id;
        this.status = ResponseModel.StatusReport.processing;
        this.percent = 0;
    }

    public TaskModel() {
    }
}
