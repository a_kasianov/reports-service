package host.apppf.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResponseModel {

    private boolean success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private StatusReport status;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonProperty("report_id")
    private int reportId;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int percent;

    public ResponseModel(boolean success, String message, int reportId) {
        this.success = success;
        this.message = message;
        this.reportId = reportId;
    }

    public ResponseModel(boolean success) {
        this.success = success;
    }

    public ResponseModel() {
    }

    public enum StatusReport {
        processing,
        completed
    }
}
