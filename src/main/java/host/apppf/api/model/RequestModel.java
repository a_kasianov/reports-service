package host.apppf.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class RequestModel {

    @JsonProperty("operator_id")
    @NotNull(message = "Field operator_id is required")
    private String operatorId;

    @JsonProperty("operator_key")
    @NotNull(message = "Field operator_key is required")
    private String operatorKey;

    private TypeReport type;

    @JsonProperty("report_id")
    private int reportId;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date period;
    private int kpi;

    public enum TypeReport {
        statistics,
        analytics,
        products
    }
}
